""" This helps people get the Gimli That Axe achievement in Killing Floor """

import ctypes
import os
import sys
import dns.resolver
import python_hosts
import requests
from bottle import run, route, request, response, error

@route('/ISteamUserStats/GetPlayerAchievements/v0001')
@route('/ISteamUserStats/GetPlayerAchievements/v0001/')
def magic():
    """ Catch the API server requests and reformat them """
    response.content_type = 'text/html'
    # Swap out url to point to ip of api.steampowered.com
    url = request.url.replace('api.steampowered.com', get_real_ip())
    # Set hosts header so the webserver responds appropriately
    headers = dict(request.headers)
    headers['Host'] = 'api.steampowered.com'
    # Get the result
    api_req = requests.get(url=url, headers=headers)
    # Reformat it to match what the game expects
    data = api_req.text
    data = data.replace('"success":true', '"success": true')
    data = data.replace('"apiname":', '"apiname": ')
    data = data.replace('"achieved":', '"achieved": ')
    data = data.replace(',{', ',{\n')
    data = data.replace('[', '[\n')
    data = data + '\n'
    # Set Headers
    response.status = api_req.status_code
    response.content_type = api_req.headers['Content-Type']
    response.expires = api_req.headers['Expires']
    return data

@error(404)
#pylint: disable=unused-argument
def proxy(bottle_err):
    """ Proxy all other requests to the api so they still work """
    response.content_type = 'text/html'
    # Swap out url to point to ip of api.steampowered.com
    url = request.url.replace('api.steampowered.com', get_real_ip())
    # Set hosts header so the webserver responds appropriately
    headers = dict(request.headers)
    headers['Host'] = 'api.steampowered.com'
    # Get the result
    api_req = requests.get(url=url, headers=headers)
    response.status = api_req.status_code
    response.content_type = api_req.headers['Content-Type']
    response.expires = api_req.headers['Expires']
    return api_req.text

def hosts_file_set():
    """ Check to see if the hosts file is configured correctly """
    hosts = python_hosts.Hosts()
    for entry in hosts.entries:
        if entry.address == "127.0.0.1" and "api.steampowered.com" in entry.names:
            return True
    return False

def get_real_ip():
    """ Get the real IP for api.steampowered.com from DNS, skipping our hosts file """
    ip_addr = dns.resolver.query("api.steampowered.com", 'A')[0]
    return f"{ip_addr}"

def is_elevated_user():
    '''
    Checks to see if you're running as admin for port 80 access
    '''
    try:
        return os.getuid() == 0
    except AttributeError:
        return ctypes.windll.shell32.IsUserAnAdmin() != 0

if __name__ == "__main__":
    if not hosts_file_set():
        print("WARNING: Hosts file is not pointing to 127.0.0.1 - this tool will not work!")
        input("Please press any key to end the program")
        sys.exit(1)

    if not is_elevated_user():
        print("ERROR: NOT RUNNING AS ADMIN!")
        print("This application needs to run on port 80 in order to function.")
        print("To use that port, it must be running as admin")
        print("Please right-click and Run as Administrator if it doesn't work")
        print("WARNING: NOT RUNNING AS ADMIN!")
        print("\n\n\n")

    print()
    run(host='localhost', port=80)
