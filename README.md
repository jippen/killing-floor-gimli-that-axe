Killing Floor - Gimli That Axe
==============================

This is a tool to help fix The achievement "Gimli That Axe" in Killing Floor. 

Unlike the original script that this was based on, however, it doesn't require someone else to maintain a heroku instance indefinately in order to continue to function. Additionally, I'm not a fan of the whole "Just download this exe and trust me" thing when instead you can just have open source code and a transparent compiling and release process.

Instructions
------------

Edit your hosts file to contain the following line:

```
127.0.0.1 api.steampowered.com
```

Then launch KF_GimliThatAxe.exe, before launching Killing Floor.

Once the achievement pops, close KF_GimliThatAxe.exe and remove that line from your hosts file so that apps can talk normally to steam once more.


Credits
-------

Jippen for the creation of this script
Leeson for the original research and [script](https://steamcommunity.com/sharedfiles/filedetails/?id=2144450008)
